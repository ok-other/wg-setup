# WireGuard quick setup on Ubuntu 20.04

- first of all be sure you run all comannds as root:
```
sudo -i
```

- install wireguard to system:
```
apt install wirequard resolvconf
```

- copy scripts from this repository:
```
git clone https://gitlab.com/ok-other/wg-setup.git /etc/wireguard/bin
```
- check config.ini file placed at /etc/wireguard/bin/config.ini to set first 4 bytes of VPN network
- set up server configuration:
```
/etc/wireguard/bin/server-setup
```

## Next step to create clients config files
- create confir for new client <c_name> (<c_name> - is the suffix file name):
```
/etc/wireguard/bin/new-config client_1
```
